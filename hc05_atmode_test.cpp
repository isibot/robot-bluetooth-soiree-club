// use mbed-os-5
#include "mbed.h"

//              D1,    D0
RawSerial pc(USBTX, USBRX);
//            TX, RX
RawSerial bt(D10, D2);

// Nucleo Led, used to Debug purpose
DigitalOut LED(LED1);

void bt_callback()
{
    while (bt.readable())
        pc.putc(bt.getc());
    // if LED blinks, this means that the callback has been called !
    LED = !LED;
}

void pc_callback() {
    while (pc.readable()) 
        bt.putc(pc.getc());
}

// This program needs to have the HC-05 in AT mode (pin Enable has to be set to 1)
// Once the initialization has been done, you can enter AT command (in your terminal) 
// to modify the receptor properties or ask it to give specific information like 
// bluetooth address, pin code...
// See the datasheet of the HC-05 for further informations on the AT mode.
// NOTE: chinese version of the HC-05 doesn't have proper AT mode...
int main()
{
    char ch;
    pc.baud(9600);
    // attach callbacks to the event "data available on the serial port of bt/pc"
    bt.attach(bt_callback);
    pc.attach(pc_callback);

    // visual indication of the running program
    for (int i=0; i < 6; i++)
        LED = !LED, wait_ms(40);

    // Test all 'standard' baudrates to see the one the HC-05 use 
    for (int e : {9600, 14400, 19200, 28800, 38400, 56000, 57600, 115200})
    {
        bt.baud(e);
        pc.printf("current bd: %d\n", e);
        wait_ms(100);
        
        bt.putc('A');
        bt.putc('T');
        bt.putc('\n');

        wait_ms(100);
        bt.putc('A');
        bt.putc('T');
        bt.putc('\r');
        bt.putc('\n');
        wait_ms(100);
    }

    // do nothing here because isr will be trigered
    while (1) {}
    
    return 0;
}