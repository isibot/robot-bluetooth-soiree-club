// use mbed-os-5
#include "mbed.h"

#define V_MAX 0.7

//            TX, RX
RawSerial bt(D10, D2);

// Nucleo Led, used to Debug purpose
DigitalOut LED(LED1);

// Abstract the speed of the two engines
// Value between 0 and 1
PwmOut speed_A(D5);
PwmOut speed_B(D6);

DigitalOut mot_A_1(D3);
DigitalOut mot_A_2(D4);

DigitalOut mot_B_1(D7);
DigitalOut mot_B_2(D8);

// This program intends to receive 'z', 'q', 's' and 'd' to control the robot (as in videogames).
int main()
{
    char c = '\0';
    for (int i=0; i < 6; i++)
        LED = !LED, wait_ms(40);

    // Initialization of the bt Serial to 9600 baud
    // NOTE: higher baudrates seems to create issues with our chinese HC-05/HC-06
    bt.baud(9600);

    // Initialization of speed
    speed_A = 0.4;
    speed_B = 0.4;

    // 2x H-Bridge configured in stop position
    mot_A_1 = 0;
    mot_A_2 = 0;
    mot_B_2 = 0;
    mot_B_1 = 0;

    // At this point, the state of the first (A) engine is :
    //                    +
    //           ---------|---------
    //          |                   |
    //          *                   *
    //  A_1=0  /                   /   A_2=0
    //          *                   *
    //          |--------(M)--------|
    //          *                   *
    //  A_2=0  /                   /   A_1=0
    //          *                   *
    //          |                   |
    //           -------|GND|-------
    // The circuit is open, thus no current goes through the engine.
    // B engine is in the same state.

    while (1) 
    {
        // if data is available to read on the current cycle
        if (bt.readable()) 
        {
            // read the top character of the stream
            c = bt.getc();

            // actualize speed variation
            if (c == 'z' || c == 's' || c == 'd' || c == 'q') {
                if (speed_A >= V_MAX) ;
                else {
                    speed_A = speed_A + 0.1;
                    speed_B = speed_B + 0.1;
                }
            }

            // specifically depending on the character, do ...
            switch(c)
            {
            // go forward
            case 'z':
                // NOTE: We wants to avoid short-circuits situation where :
                //                    +
                //           ---------|---------
                //          |                   |
                //          *                   *
                //   A_1=1  |                   |  A_2=1
                //          *                   *
                //          |--------(M)--------|
                //          *                   *
                //   A_2=1  |                   |  A_1=1
                //          *                   *
                //          |                   |
                //           -------|GND|-------
                // Current doesn't go through the engine... (it smells the brulée)

                // So, first we have to cut down the pair which isn't used :
                // (because mot_A_2 could have been previously set to 1)
                mot_A_2 = 0;
                // Then, we can activate the other one :
                mot_A_1 = 1;

                // engine is now in state :
                //                    +
                //           ---------|---------
                //          |                   |
                //          *                   *
                //   A_1=1  |                  /  A_2=0
                //          *       ---->       *
                //          |--------(M)--------|
                //          *                   *
                //   A_2=0 /                    |  A_1=1
                //          *                   *
                //          |                   |
                //           -------|GND|-------

                // same for the second engine
                mot_B_2 = 0;
                mot_B_1 = 1;
                break;

            // go backward
            case 's':
                // same logic used with 'z' but in reverse
                mot_A_1 = 0;
                mot_A_2 = 1;

                // engine is in state :
                //                    +
                //           ---------|---------
                //          |                   |
                //          *                   *
                //   A_1=0 /                    |  A_2=1
                //          *       <----       *
                //          |--------(M)--------|
                //          *                   *
                //   A_2=1  |                  /   A_1=0
                //          *                   *
                //          |                   |
                //           -------|GND|-------
                
                // same for B
                mot_B_1 = 0;
                mot_B_2 = 1;
                break;

            // go right
            case 'd':
                mot_A_2 = 0;
                mot_A_1 = 1;

                mot_B_1 = 0;
                mot_B_2 = 1;
                break;

            // go left
            case 'q':
                mot_A_1 = 0;
                mot_A_2 = 1;

                mot_B_2 = 0;
                mot_B_1 = 1;
                break;

            // other characters --> stop the robot
            // NOTE: be aware that if CR/LF are inserted at the end of the message like:
            //       'z''\r''\n'
            //       '\r' and '\n' will fall into that cases, which will prevent the robot to move...
            //       Use software like CuteCom to prevent this kind of things to happened when testing !
            default:
                mot_A_1 = 0;
                mot_A_2 = 0;

                mot_B_2 = 0;
                mot_B_1 = 0;
                break;
            }
            wait(0.2);
        }
        // if no message was sent
        else {
            // if the speed is ever too slow
            if (speed_A <= 0.4) {
                // let's stop the engines
                mot_A_1 = 0;
                mot_A_2 = 0;
                mot_B_2 = 0;
                mot_B_1 = 0;
            }
            // otherwise decrease (slowly) the current speed
            else {
                speed_A = speed_A - 0.1;
                speed_B = speed_B - 0.1;
            }
            wait(0.2);
        }
    }
    return 0;
}
